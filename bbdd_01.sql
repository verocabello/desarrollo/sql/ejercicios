﻿USE videoclub;

SELECT * FROM peliculas;

SELECT COUNT(*) AS TotalPeliculas FROM peliculas;

SELECT genero, COUNT(*) AS TotalPeliculas FROM peliculas GROUP BY genero;

INSERT INTO peliculas VALUES
  ('P001','Rebelión en las Ondas','Comedia',1987,NULL),
  ('P002','Rocky','Drama',1976,NULL),
  ('P003','El Exorcista','Terror',1973,NULL),
  ('P004','Ronin','Policiaca',1998,NULL),
  ('P005','Rocky II','Drama',1979,NULL),
  ('P006','Cyrano de Bergeraç','Drama',1990,NULL),
  ('P007','Rocky III','Drama',1982,NULL),
  ('P008','El Resplandor','Terror',1980,NULL),
  ('P009','Sin Perdón','Western',1992,NULL);

INSERT INTO socios VALUES
  ('S001','10101010A','Fernando','Alonso','Calle Rueda','985332211','02/06/1980'),
  ('S002','20202020B','Elmer','Benett','Avda Castilla','915852266','05/12/1985'),
  ('S003','30303030C','Sofía','Loren','Plaza Mayor','942335544','07/08/1903'),
  ('S004','40404040D','Mar','Flores','Calle Alegría','914587895','31/05/1978'),
  ('S005','50505050E','Tamara','Torres','Calle Perejil','935784578','24/03/1975'),
  ('S006','11111111F','Federico','Trillo','Calle Honduras','942225544','29/04/1950'),
  ('S007','11223344G','Marina','Castaño','Avda Jeta','916589878','15/01/1957'),
  ('S008','72727272H','Diego','Tristán','Calle Galicia','942353535','19/10/1979'),
  ('S009','25252525J','Andrei','Shevchenko','Calle Milán','915544545','26/03/1982');

INSERT INTO prestamos VALUES
  (001,'S001','P001'),
  (002,'S002','P003'),
  (003,'S003','P004'),
  (004,'S005','P006'),
  (005,'S007','P001'),
  (006,'S008','P002'),
  (007,'S008','P003'),
  (008,'S009','P001');

INSERT INTO prestamos VALUES
  (009,'S004','P005'),
  (010,'S006','P007'),
  (011,'S001','P008'),
  (012,'S002','P009');
  


