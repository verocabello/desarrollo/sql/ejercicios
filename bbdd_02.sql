﻿CREATE DATABASE IF NOT EXISTS estaciones;

USE estaciones;

CREATE OR REPLACE TABLE estacion(
  id mediumint UNSIGNED,
  latitud varchar(14),
  longitud varchar(15),
  altitud mediumint,
  PRIMARY KEY(id)
);

CREATE OR REPLACE TABLE muestra(
  idestacion mediumint UNSIGNED,
  fecha date,
  temperaturaMinima tinyint,
  temperaturaMaxima tinyint,
  precipitaciones smallint UNSIGNED,
  humedadMinima tinyint,
  humedadMaxima tinyint,
  velocidadMinima smallint UNSIGNED,
  velocidadMaxima smallint UNSIGNED,
  PRIMARY KEY(idestacion,fecha)
);

ALTER TABLE muestra
  ADD FOREIGN KEY (idestacion)
  REFERENCES estacion(id);