﻿CREATE DATABASE IF NOT EXISTS libros;

USE libros;

CREATE OR REPLACE TABLE libro(
  claveLibro int,
  titulo varchar(100),
  idioma varchar(100),
  formato varchar(100),
  categoria varchar(100),
  claveEditorial int,
  PRIMARY KEY(claveLibro)
);

CREATE OR REPLACE TABLE tema(
  claveTema int,
  nombreTema varchar(100),
  PRIMARY KEY(claveTema)
);

CREATE OR REPLACE TABLE autor(
  claveAutor int,
  nombreAutor varchar(100),
  PRIMARY KEY(claveAutor)
);

CREATE OR REPLACE TABLE editorial(
  claveEditorial int,
  nombreEditorial varchar(100),
  direccion varchar(100),
  telefono int,
  PRIMARY KEY(claveEditorial)
);

CREATE OR REPLACE TABLE ejemplar(
  claveLibro int,
  numeroOrden int,
  edicion varchar(100),
  ubicacion varchar(100),
  PRIMARY KEY(claveLibro,numeroOrden)
);

CREATE OR REPLACE TABLE socio(
  claveSocio int,
  nombreSocio varchar(100),
  direccion varchar(100),
  telefono int,
  categoria varchar(100),
  PRIMARY KEY(claveSocio)
);

CREATE OR REPLACE TABLE trata_sobre(
  claveLibro int,
  claveTema int,
  PRIMARY KEY(claveLibro,claveTema)
);

CREATE OR REPLACE TABLE escrito_por(
  claveLibro int,
  claveAutor int,
  PRIMARY KEY(claveLibro,claveAutor)
);